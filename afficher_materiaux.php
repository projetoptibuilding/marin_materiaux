<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
        <header>
        </header>
                
        <section>
            <h1>La base de données des matériaux</h1>
<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=projet_optibuilding;charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
?>
        <p>
            <table>
            <caption></caption>
            <thead>
                <tr>
                    <th>Code Matériau</th>
                    <th>MAJ</th>
                    <th>CUPI</th>
                    <th>Poste</th>
                    <th>Type Matériau</th>
                    <th>Libellé</th>
                    <th>Fabriquant</th>
                    <th>Prix unitaire</th>
                    <th>Unité</th>
                    <th>Durée de vie</th>
                    <th>Taux d'entretien annuel</th>
                    <th>Taux de remplacement</th>
                </tr>
            </thead>
                     
            <tbody>
<?php   $messagesParPage=20; //Nous allons afficher 20 entrées par page

        $retour_total=$bdd->query('SELECT COUNT(*) AS total FROM materiaux');
        $donnees_total=$retour_total->fetch(PDO::FETCH_ASSOC);
        $total=$donnees_total['total']; //On récupère le total pour le placer dans la variable $total.
         
        //Nous allons maintenant compter le nombre de pages.
        $nombreDePages=ceil($total/$messagesParPage);
         
        if(isset($_GET['page'])) // Si la variable $_GET['page'] existe... Afficher une erreur si jamais on entre une mauvaise URL
        {
             $pageActuelle=intval($_GET['page']);
         
             if($pageActuelle>$nombreDePages) // Si la valeur de $pageActuelle (le numéro de la page) est plus grande que $nombreDePages...
             {
                  $pageActuelle=$nombreDePages;
             }
        }
        else
        {
             $pageActuelle=1; // La page actuelle est la n°1    
        }
         
        $premiereEntree=($pageActuelle-1)*$messagesParPage; // On calcul la première entrée à lire

        $affiche=$bdd->query('SELECT* FROM materiaux ORDER BY code_mat LIMIT '.$premiereEntree.', '.$messagesParPage.'');
    
       
        while($donnes=$affiche->fetch())
        {?>
               <tr>
                   <td><?php echo $donnes['code_mat']; ?></td> 
                   <td><?php echo $donnes['MAJ_mat']; ?></td>
                   <td><?php echo $donnes['CUPI_mat']; ?></td>
                   <td><?php echo $donnes['poste_mat']; ?></td>
                   <td><?php echo $donnes['type_mat']; ?></td>
                   <td><?php echo $donnes['libelle_mat']; ?></td>
                   <td><?php echo $donnes['fabricant_mat']; ?></td>
                   <td><?php echo $donnes['prix_unitaire_mat']; ?></td>
                   <td><?php echo $donnes['unite_mat']; ?></td>
                   <td><?php echo $donnes['duree_de_vie_mat']; ?></td>
                   <td><?php echo $donnes['taux_entretien_mat']; ?></td>
                   <td><?php echo $donnes['taux_remplacement_mat']; ?></td>
               </tr>
               
        <?php }
         echo '<p align="center">Page : '; //Pour l'affichage, on centre la liste des pages
            for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
            {
                 //On va faire notre condition
                 if($i==$pageActuelle) //Si il s'agit de la page actuelle...
                 {echo ' [ '.$i.' ] ';}	
                 else //Sinon...
                 {echo ' <a href="afficher_materiaux.php?page='.$i.'">'.$i.'</a> ';}
            }
            echo '</p>';
        ?>
                </tbody> 
             </table> 
        </p>
            
            <a href="insertion_mat.php"><input type='button' value='Ajouter un matériau'/></a>
            <a href="modif_ret_mat.php"><input type='button' value='Modifier ou Retirer un matériau'/></a>
                    
        </section>
                             
                <footer>
             
                </footer>
                             
            </div>
        </body>
    </html>