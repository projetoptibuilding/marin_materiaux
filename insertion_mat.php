<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
    
    <body>
     
    <header>
    </header>
                
    <section>
        <h1>Créer un nouveau matériau dans la base de données</h1>
<!-- Cette page comporte un formulaire qui permet d'insérer un nouveau matériau dans la base de donnés, les infos entréees dans le
formulaire sont traitées par le code php sur la page inserer_mat.php -->
            
        <form method='post' action='inserer_mat.php'>
        <p>
        <fieldset>
            <legend>Pour créer un matériau</legend>
            
            <label for='code_mat'>Code du matériau</label>
            <input type='text' id='code_mat' name='code_mat'/></br></br>
            
            <label for='poste'>Poste</label>
            <select name="poste" id="poste">
                <option value="sol">Sol</option>
                <option value="veture">Vêture</option>
                <option value="toiture">Toiture</option>
            </select>
            </br></br>
            
            <label for='type_materiau'>Type du matériau</label>
            <input type='text' id='type_materiau' name='type_materiau'/></br></br>
            
            <label for='libelle'>Libellé</label>
            <textarea name='libelle' rows="4" cols="45" placeholder='Libellé' ></textarea>
            
            <label for='fabricant'>Fabricant</label>
            <input type='text' id='fabricant' name='fabricant'/></br></br>
            
            <label for='prix_unitaire'>Prix unitaire<em>(Utiliser des points et non pas des virgules)</em></label>
            <input type='numbre' id='prix_unitaire' name='prix_unitaire'/>
            
            <select name="unite" id="unite">
                <option value="mL">mL</option>
                <option value="m2" selected="selected">m2</option>
            </select></br></br>
            
            <label for='duree_de_vie'>Durée de vie <em>(en années)</em></label>
            <input type='text' id='duree_de_vie' name='duree_de_vie'/></br></br>
            
            <label for='taux_entretien'>Taux d'entretien annuel <em>(en pourcentage)</em></label>
            <input type='number' id='taux_entretien' name='taux_entretien' value="3" min="0" step="0.1"/></br></br>
            
            <label for='taux_remplacement'>Taux de remplacement <em>(en pourcentage)</em></label>
            <input type='number' id='taux_remplacement' name='taux_remplacement' value="100" min="0"/></br></br>
            
            <label for='taux_deconstruction'>Taux de déconstruction <em>(en pourcentage)</em></label>
            <input type='number' id='taux_deconstruction' name='taux_deconstruction' value ="3" min="0"/></br></br>
            
            
            <input type='submit' value='Créer le matériau'/>
            <input type='reset' value='Remettre le formulaire à zéro'/>
            </br></br>
                
        </fieldset>
        </p>
        </form>
            
        <a href="afficher_materiaux.php"><input type='button' value='Retour à la table'/></a>                  
 
     </section>
                             
    <footer>
    </footer>
                             
    </body>
</html>
