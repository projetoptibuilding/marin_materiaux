<!-- Cette page traite les infos du formulaire de création d'un nouveau matériau
    Connexion à la base de données, préparation de l'insertion puis insertion de la nouvelle entrée dans la base de données.
    On se redirige automatiquement vers la page d'affichage des matériaux
-->

<?php try {$bdd= new PDO ('mysql:host=localhost;dbname=projet_optibuilding;charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
          
        $req=$bdd->prepare('INSERT INTO materiaux(code_mat, MAJ_mat, CUPI_mat, poste_mat, type_mat, libelle_mat, fabricant_mat, unite_mat, 
                            prix_unitaire_mat, duree_de_vie_mat, taux_entretien_mat, taux_remplacement_mat)
                                       VALUES(:code_mat, :MAJ_mat, :CUPI_mat, :poste_mat, :type_mat, :libelle_mat, :fabricant_mat, :unite_mat,
                                       :prix_unitaire_mat, :duree_de_vie_mat, :taux_entretien_mat, :taux_remplacement_mat)');
        $req->execute(array('code_mat'=>htmlentities($_POST['code_mat']), 'MAJ_mat'=>htmlentities($_POST['MAJ_mat']), 
                            'CUPI_mat'=>htmlentities($_POST['CUPI_mat']), 'poste_mat'=>htmlentities($_POST['poste_mat']),
                            'type_mat'=>htmlentities($_POST['type_mat']),'libelle_mat_mat'=>htmlentities($_POST['libelle_mat']),
                            'fabricant'=>htmlentities($_POST['fabricant_mat']), 'unite'=>htmlentities($_POST['unite_mat']),
                            'prix_unitaire_mat'=>htmlentities($_POST['prix_unitaire_mat']), 
                            'duree_de_vie_mat'=>htmlentities($_POST['duree_de_vie_mat']),
                            'taux_entretien_mat'=>htmlentities($_POST['taux_entretien_mat']),
                            'taux_remplacement_mat'=>htmlentities($_POST['taux_remplacement_mat']),
                            ));
                
                header('Location:afficher_materiaux.php');
?>