<!-- Cette page affiche un formulaire de modification d'un matériau de l abase de données.
On récupère l'id du matériau via l'URL puis on affiche un formulaire préremplit avec les infos sur le matériau-->
<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
        <section>
        <p>Cette page vous permet de mofifier un matériau dans la base de données.</p>                   
            <form method='post' action='modifier_mat.php'>
                <p>
                <fieldset>
<?php  try {$bdd= new PDO ('mysql:host=localhost;dbname=projet_optibuilding;charset=utf8', 'root', '',
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        $id_mat=htmlentities($_GET['mat']);
        
        $affiche=$bdd->query('SELECT* FROM materiaux WHERE id_mat='.$id_mat.'');
        $mat=$affiche->fetch()
?>                    
                <legend>Pour modifier un matériau</legend>
                
                <input type="hidden" name="id_mat" value="<?php echo $id_mat; ?>"/>
                
                <label for='code_mat'>Code du matériau</label>
                <input type='text' id='code_mat' name='code_mat' value="<?php echo $mat['code_mat']; ?>"/></br></br>
                
                <label for='poste'>Poste</label>
                <select name="poste" id="poste">
                    <option value="sol">Sol</option>
                    <option value="veture">Vêture</option>
                    <option value="toiture">Toiture</option>
                    <option value="autre">Autre</option>
                </select>
                </br></br>
                
                <label for='type_materiau'>Type du matériau</label>
                <input type='text' id='type_materiau' name='type_materiau' value='<?php echo $mat['type_materiau']; ?>'/></br></br>
                
                <label for='libelle'>Libellé</label>
                <textarea name='libelle' rows="4" cols="45"><?php echo $mat['libelle']; ?></textarea>
                
                <label for='fabricant'>Fabricant</label>
                <input type='text' id='fabricant' name='fabricant' value='<?php echo $mat['fabricant']; ?>'/></br></br>
                
                <label for='prix_unitaire'>Prix unitaire<em>(Utiliser des points et non pas des virgules)</em></label>
                <input type='numbre' id='prix_unitaire' name='prix_unitaire' value='<?php echo $mat['prix_unitaire']; ?>'/>
                
                <select name="unite" id="unite">
                    <option value="mL">mL</option>
                    <option value="m2" selected="selected">m2</option>
                    <option value="autre">Autre</option>
                </select></br></br>
                
                <label for='duree_de_vie'>Durée de vie <em>(en années)</em></label>
                <input type='text' id='duree_de_vie' name='duree_de_vie' value='<?php echo $mat['duree_de_vie']; ?>'/></br></br>
                
                <label for='taux_entretien'>Taux d'entretien annuel <em>(en pourcentage)</em></label>
                <input type='number' id='taux_entretien' name='taux_entretien' value="<?php echo $mat['taux_entretien']; ?>" min="0" step="0.01"/></br></br>
                
                <label for='taux_remplacement'>Taux de remplacement <em>(en pourcentage)</em></label>
                <input type='number' id='taux_remplacement' name='taux_remplacement' value="<?php echo $mat['taux_remplacement']; ?>" min="0" step="0.01"/></br></br>
                
                <label for='taux_deconstruction'>Taux de déconstruction <em>(en pourcentage)</em></label>
                <input type='number' id='taux_deconstruction' name='taux_deconstruction' value ="<?php echo $mat['taux_deconstruction']; ?>" min="0" step="0.01"/></br></br>
                
                
                <input type='submit' value='Modifier'/>
                <input type='reset' value='Remettre à zéro'/>
                </br></br>
                        
                    </fieldset>
                </p>
                </form>
                
                 <a href="modif_ret_mat.php"><input type='button' value='Annuler'/></a>
                 <a href="afficher_materiaux.php"><input type='button' value='Retour à la table'/></a>
        </section>
                         
        <footer>         
        </footer>

    </body>
</html>
