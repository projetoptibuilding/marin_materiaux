<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
    <header>
    </header>
                
    <section>
        <h1>La base de données des matériaux</h1>
<!-- Cette page offre la possibilité de choisir un matériau que l'on veut modifier ou retirer de notre base de données
On se connecte à la BDD qui contient tous les matériaux et on les affiche dans un tableau, chaque ligne contient deux boutons : un pour modifier les propriétés,
l'autre pour retirer le matériau de la liste-->
                    
<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=projet_optibuilding;charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
        
        $affiche=$bdd->query('SELECT* FROM materiaux ORDER BY id_mat LIMIT 0,30');
?>
               
        <a href="afficher_materiaux.php"><input type='button' value='Retour à la table'/></a>

        <p>
        <table>
            <caption>Matériaux</caption>
            <thead>
                <tr>
                    <th></th>
                    <th>Code Matériau</th>
                    <th>Poste</th>
                    <th>Type Matériau</th>
                    <th>Libellé</th>
                    <th>Fabriquant</th>
                    <th>Prix unitaire</th>
                    <th>Unité</th>
                    <th>Durée de vie</th>
                    <th>Taux d'entretien annuel</th>
                    <th>Taux de remplacement</th>
                    <th>Taux de déconstruction</th>
                </tr>
                </thead>
                     
                <tbody>
                                            
<?php   while($donnes=$affiche->fetch())
{?>
                <tr>
                    <td><a href="modification_mat.php?mat=<?php echo $donnes['id_mat']; ?>"><input type='button' value='Modifier'/></a>
<!-- On atteind la page de mofification de matériau en faisant passer l'id du matériau par l'URL -->                   
                        <a href="retirer_mat.php?mat=<?php echo $donnes['id_mat']; ?>"><input type='button' value='Retirer'
                                onclick="return confirm('Voulez-vous vraiment supprimer ce matériau ? Cette action est irréversible');"/></a>
<!-- On atteind le code de suppression de l'entrée choisie, au moment du clic, une page d'avertissement d'affiche demande de confirmer l'action -->                                
                    </td>
                    <td><?php echo $donnes['code_mat']; ?></td>
                    <td><?php echo $donnes['poste']; ?></td>
                    <td><?php echo $donnes['type_materiau']; ?></td>
                    <td><?php echo $donnes['libelle']; ?></td>
                    <td><?php echo $donnes['fabricant']; ?></td>
                    <td><?php echo $donnes['prix_unitaire']; ?></td>
                    <td><?php echo $donnes['unite']; ?></td>
                    <td><?php echo $donnes['duree_de_vie']; ?></td>
                    <td><?php echo $donnes['taux_entretien']; ?></td>
                    <td><?php echo $donnes['taux_remplacement']; ?></td>
                    <td><?php echo $donnes['taux_deconstruction']; ?></td>
                </tr>
<?php } ?>
                </tbody>
        </table>
        </p>
        </br></br>
        
        <p><a href="afficher_materiaux.php"><input type='button' value='Retour à la table'/></a></p>
        
                
    </section>
                             
    <footer></footer>                             
    </body>
</html>