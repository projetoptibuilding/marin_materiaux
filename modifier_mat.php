<!-- Cette page exécute la modification d'un matériau dans la base de données -->

<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=projet_optibuilding;charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
         
        $id=htmlentities($_POST['id_mat']);
        $req=$bdd->prepare('UPDATE materiaux SET code_mat= :code_mat, poste= :poste, type_materiau= :type_materiau, libelle= :libelle, fabricant= :fabricant,
                            unite= :unite, prix_unitaire= :prix_unitaire, duree_de_vie= :duree_de_vie, taux_entretien= :taux_entretien, taux_remplacement= :taux_remplacement,
                            taux_deconstruction= :taux_deconstruction
                            WHERE id='.$id.'');
        $req->execute(array(
            'code_mat'=>htmlentities($_POST['code_mat']),
            'poste'=>htmlentities($_POST['poste']),
            'type_materiau'=>htmlentities($_POST['type_materiau']),
            'libelle'=>htmlentities($_POST['libelle']),
            'fabricant'=>htmlentities($_POST['fabricant']),
            'unite'=>htmlentities($_POST['unite']),
            'prix_unitaire'=>htmlentities($_POST['prix_unitaire']),
            'duree_de_vie'=>htmlentities($_POST['duree_de_vie']),
            'taux_entretien'=>htmlentities($_POST['taux_entretien']),
            'taux_remplacement'=>htmlentities($_POST['taux_remplacement']),
            'taux_deconstruction'=>htmlentities($_POST['taux_deconstruction']) ));
        
        header('Location:afficher_materiaux.php');
?>

