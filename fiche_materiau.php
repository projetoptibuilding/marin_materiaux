<?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
        $mat=htmlentities($_GET['mat']);

        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        
        $affiche=$bdd->query('SELECT* FROM articles WHERE code_article="'.$mat.'" AND id_piece='.$id_piece.' AND id_scenario='.$id_scenario.'');
        $mat=$affiche->fetch()
?>

<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="DesignOptibuilding.css"/>
    </head>
        
    <body>
        <section>
        <p>Fiche matériau <?php echo $mat['code_article'];?></p>

                
                CUPI : <?php echo $mat['CUPI_article'];?></br></br>
                
                Type du matériau : <?php echo $mat['type_materiau']; ?></br></br>
                
                Libellé : <?php echo $mat['libelle']; ?></br></br>
                
                Fabricant :<?php echo $mat['fabricant']; ?></br></br>
                           
                Prix unitaire : <?php echo $mat['prix_unitaire'];?> €</br></br>
                
                Durée de vie : <?php echo $mat['duree_de_vie']; ?> années</br></br>
                
                Taux d'entretien annuel : <?php echo $mat['taux_entretien']; ?></br></br>
                
                Taux de remplacement : <?php echo $mat['taux_remplacement']; ?></br></br>
        </p>
        
        <p><form method='post' action='../articles/modification_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='hidden' name='id_article' value='<?php echo $mat['id_article'];?>'/>
        <input type='submit' value='Modifier' onclick="return confirm('Attention vous allez quitter l analyse du calcul.\nOK pour continuer\nAnnuler pour rester sur la page')"/></p>
        
        
        <p><a href='../calcul/analyse.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='button' value='Retour'/></a></p>
        
        </section>

    </body>
</html>
